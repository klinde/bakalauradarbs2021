import os
import tkinter as tk
import sys
import tweepy
import datetime
from tweepy import API 
from tweepy import Cursor
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from textblob import TextBlob
import twitter_credentials
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import openpyxl
import re
import os.path
from os import path
import pathlib

auth = OAuthHandler(twitter_credentials.CONSUMER_KEY, twitter_credentials.CONSUMER_SECRET)
auth.set_access_token(twitter_credentials.ACCESS_TOKEN, twitter_credentials.ACCESS_TOKEN_SECRET)
api = tweepy.API(auth,wait_on_rate_limit=True)
today = datetime.date.today()
savedate = today.strftime("%b-%d-%Y")    
def get_tweets():
        
        atslegasvardi = open('keywords.txt', 'r', encoding="utf8")
        count = 0
        for atslegasvards in atslegasvardi:
                count += 1
                
                tweets_list = tweepy.Cursor(api.search, q=atslegasvards.strip(),tweet_mode='extended', lang='lv', ).items(50)
                output = []
                for tweet in tweets_list:
                    text = tweet._json["full_text"]
                    print(text)
                    favourite_count = tweet.favorite_count
                    retweet_count = tweet.retweet_count
                    created_at = tweet.created_at
                    author = tweet.author.name
                    line = {'teksts' : text, 'favourite_count' : favourite_count, 'retweet_count' : retweet_count, 'created_at' : created_at, 'author' : author}
                    output.append(line)
                    df = pd.DataFrame(output, columns = ['teksts', 'favourite_count', 'retweet_count', 'created_at', 'author'])
                df.to_csv(savedate + '.csv', mode='a', encoding='utf-16')
        atslegasvardi.close()
        
def open_keywords():
        fileName = "keywords.txt"
        os.system("start " + fileName)

root = tk.Tk()
root.geometry('500x50')
root.configure(background='white')
root.title("Twitter datu meklesana")
frame = tk.Frame(root)
frame.pack()

tweets_bt = tk.Button(frame,
                   text="Meklet tvitus",
                   command=get_tweets, bg="light blue", fg="white", width=20,height=10, highlightthickness = 0, bd = 0)
tweets_bt.pack(side=tk.LEFT)
keywords_bt = tk.Button(frame,
                   text="Atslegas vardi", highlightthickness = 0, bd = 0, width=20,height=10,
                   command=open_keywords)
keywords_bt.pack(side=tk.LEFT)
button = tk.Button(frame, 
                   text="Beigt", 
                   fg="red", width=20,height=10, highlightthickness = 0, bd = 0,
                   command=quit)
button.pack(side=tk.LEFT)

root.mainloop()
